
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Integration with 3rd party notifiers



INTRODUCTION
------------

WhatPlayed enables collection and display of "play events" from one or more
external sources. These events are probably music tracks being listened to on
a site owner's device but could also be video clips, movies, etc. The module
relies on 3rd party notification tools and audio/video player software to
perform event notification via a URL POST. Currently two specific notification
tools are supported:

 .) Advanced mIRC Integration Plug-in (AMIP)
     - see: http://amip.tools-for.net/wiki/
     - AMIP is free to download

 .) Brandon Fuller's Now Playing plugin
     - see: http://brandon.fuller.name/archives/hacks/nowplaying/winamp/
     - shareware, costs $15

Between these two tools, a number of player softwares are therefore applicable:
Winamp, iTunes, Windows Media Player, foobar2000, QMP. For more information see
the links to each notification tool above.



INSTALLATION
------------

 1) Place the entire "whatplayed" directory into your Drupal modules directory
    (normally ../sites/all/modules or such).

 2) Enable the WhatPlayed module by navigating to your Drupal modules admin
    page, checking the 'Enabled' chackbox, and saving the configuration.
 
For more help, see:

 http://drupal.org/documentation/install/modules-themes/modules-7



CONFIGURATION
-------------

Navigate to the ..admin/config/whatplayed page to make and save configation
values. Configuration settings are:

 .) Maximum number of recent tracks
     - how many different tracks are stored in the "recent tracks" table which
       is used for fast display of the recently played tracks block.

 .) Secret key for pinging
     - a value that is compared against incoming notifications to validate that
       the notification is from a trusted source (read: a source using the
       correct secret key).



INTEGRATION WITH 3RD PARTY NOTIFIERS
------------------------------------

WhatPlayed supports two notification tools at this time. We cannot provide any
support for installing, configuring or troubleshooting 3rd party tools. These
notes may help.

 .) Advanced mIRC Integration Plug-in
     - AMIP provides no direct way of POSTing a notification but paired with
       curl can accomplish the task. As one example, AMIP running as a plugin
       inside Winamp with curl.exe requires something like the following
       "Output string":
       
       /exec: (C:\local\bin\curl.exe) --header "X-AMIP: YOURSECRET" -d "title=$ue(%2)&album=$ue(%4)&artist=$ue(%1)&genre=$ue(%7)&kind=$ue(%ext)&year=$ue(%5)&track=$ue(%3)&file=$ue(%fn)" http://yoursite.com/whatplayed/ping/amip
       
       The example assumes that curl.exe is located in the C:\local\bin dir;
       the secret key is YOURSECRET; and the target Drupal site with WhatPlayed
       installed is rooted at http://yoursite.com/
       
       The POST string supplied by the '-d' parameter is the most critical part
       that should remain unchanged.

 .) Brandon Fuller's Now Playing







