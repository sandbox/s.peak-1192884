<?php

/**
 * @file
 * Default theme implementation to display a list of tracks.
 *
 * Available variables:
 * - $tracks: An array of tracks to be displayed.
 *
 * Each $track in $tracks contains:
 * - $track->title: The title of the track.
 * - $track->album: The album the track comes from.
 * - $track->track_number: The track number on the album.
 * - $track->artist: The track artist/author.
 * - $track->played_last: The raw timestamp the track was played at.
 *
 */
?>
<ul class="whatplayed-tracklist">
  <?php foreach ($tracks as $track): ?>
    <li class="whatplayed-track">
    	<span class="whatplayed-playedago"><?php print format_interval(time() - $track->played_last);?></span>
    	<span class="whatplayed-title"><?php print $track->title; ?></span>;
    	#<span class="whatplayed-tracknumber"><?php print $track->track_number; ?></span>
    	on
    	<span class="whatplayed-album"><?php print $track->album; ?></span>
    	by
    	<span class="whatplayed-artist"><?php print $track->artist; ?></span>
    </li>
  <?php endforeach; ?>
</ul>

